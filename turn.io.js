const { exec } = require('child_process')
const request = require('superagent')
const fs = require('fs')
const mime = require('mime')

let token = ''
const loginEndpoint = 'https://whatsapp.turn.io/v1/users/login'
const messageEndpoint = 'https://whatsapp.turn.io/v1/messages'
const mediaEndpoint = 'https://whatsapp.turn.io/v1/media'

const login = (username, password, type = 'native') => {
  return type === 'agent'
    ? agentLogin(username, password)
    : nativeLogin(username, password)
}

const nativeLogin = (username, password) => {
  const cmd = `curl -X POST --user '${username}:${password}' ${loginEndpoint}`
  return new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) console.error(`exec error: ${error}`)
      if (stderr) console.error(`stderr: ${stderr}`)
      token = JSON.parse(stdout).users[0].token
      if (token) resolve(token)
      reject(error)
    })
  })
}

const agentLogin = (username, password) => {
  const credentials = `${username}:${password}`
  const authorizationToken = Buffer.from(credentials).toString('base64')
  return new Promise((resolve, reject) => {
    request
      .post(loginEndpoint)
      .send({ user: JSON.stringify(credentials) })
      .set('Authorization', `Basic ${authorizationToken}`)
      .then(({ text }) => {
        token = JSON.parse(text).users[0].token
        if (token) resolve(token)
      })
      .catch(error => {
        reject(error)
      })
  })
}

const sendTextMessage = (body, to) => {
  return new Promise((resolve, reject) => {
    request
      .post(messageEndpoint)
      .send(
        JSON.stringify({
          preview_url: false,
          recipient_type: 'individual',
          to,
          type: 'text',
          text: {
            body
          }
        })
      )
      .set('Authorization', `Bearer ${token}`)
      .set('Content-Type', 'application/json')
      .then(response => {
        resolve(JSON.parse(response.text).messages[0].id)
      })
      .catch(error => {
        reject(error)
      })
  })
}

const sendMediaMessage = (audioUrl, to) => {
  return new Promise((resolve, reject) => {
    const audioDownloadPath = `./${Date.now()}.${audioUrl.split('.').pop()}`
    const audio = fs.createWriteStream(audioDownloadPath)
    const req = request.get(audioUrl)
    req.pipe(audio)
    audio.on('finish', function () {
      const stream = fs.createReadStream(audioDownloadPath)
      const buffers = []
      stream.on('data', function (data) {
        buffers.push(data)
      })
      stream.on('end', function () {
        const actualContents = Buffer.concat(buffers)
        const mediaMimeType = mime.getType(audioDownloadPath)
        fs.unlink(audioDownloadPath, error => {
          if (error) console.log(error)
        })
        request
          .post(mediaEndpoint)
          .send(actualContents)
          .set('Authorization', `Bearer ${token}`)
          .set('Content-Type', mediaMimeType)
          .then(response => {
            const id = JSON.parse(response.text).media[0].id
            const type = mediaMimeType.split('/')[0]
            const responseMsg = {
              recipient_type: 'individual',
              to,
              type
            }
            if (type === 'audio') {
              responseMsg.audio = { id }
            } else if (type === 'video') {
              responseMsg.video = { id }
            }
            request
              .post(messageEndpoint)
              .send(JSON.stringify(responseMsg))
              .set('Authorization', `Bearer ${token}`)
              .set('Content-Type', 'application/json')
              .then(response => {
                resolve(JSON.parse(response.text).messages[0].id)
              })
              .catch(error => {
                reject(error)
              })
          })
          .catch(error => {
            reject(error)
          })
      })
    })
  })
}

const sendMediaMessageV2 = (videoPath, to) => {
  return new Promise((resolve, reject) => {
    const cmd = `curl -X POST ${mediaEndpoint} -H 'Authorization: Bearer ${token}' -H 'Content-Type: video/mp4' --data-binary @${videoPath}`
    exec(cmd, (error, stdout, stderr) => {
      if (JSON.parse(stdout).media) {
        const id = JSON.parse(stdout).media[0].id
        const responseMsg = {
          recipient_type: 'individual',
          to,
          type: 'video',
          video: { id }
        }
        request
          .post(messageEndpoint)
          .send(JSON.stringify(responseMsg))
          .set('Authorization', `Bearer ${token}`)
          .set('Content-Type', 'application/json')
          .then(response => {
            resolve(JSON.parse(response.text).messages[0].id)
          })
          .catch(error => {
            reject(error)
          })
      }
    })
  })
}

const getMedia = id => {
  return new Promise((resolve, reject) => {
    request
      .get(`${mediaEndpoint}/${id}`)
      .set('Authorization', `Bearer ${token}`)
      .then(response => resolve(response.body))
      .catch(error => reject(error))
  })
}

module.exports = {
  login,
  sendTextMessage,
  sendMediaMessage,
  getMedia,
  sendMediaMessageV2
}
